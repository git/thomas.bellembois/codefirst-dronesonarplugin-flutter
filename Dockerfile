FROM ubuntu:jammy

RUN apt update && apt -y install git apt-transport-https wget gnupg curl unzip

RUN wget -qO- https://dl-ssl.google.com/linux/linux_signing_key.pub | gpg --dearmor -o /usr/share/keyrings/dart.gpg

RUN echo 'deb [signed-by=/usr/share/keyrings/dart.gpg arch=amd64] https://storage.googleapis.com/download.dartlang.org/linux/debian stable main' | tee /etc/apt/sources.list.d/dart_stable.list

RUN apt update && apt -y install dart

WORKDIR /opt

RUN git clone https://github.com/flutter/flutter.git -b stable

ENV PATH=$PATH:/usr/lib/dart/bin:/opt/flutter/bin

RUN flutter precache && flutter doctor

ADD https://binaries.sonarsource.com/Distribution/sonar-scanner-cli/sonar-scanner-cli-4.7.0.2747-linux.zip /opt

RUN unzip /opt/sonar-scanner-cli-4.7.0.2747-linux.zip

ENV PATH=$PATH:/opt/sonar-scanner-4.7.0.2747-linux/bin

RUN rm -Rf /var/cache/apt
